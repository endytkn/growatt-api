import requests
from endpoints import growatt_endpoints as endpoints

class GrowattApi:
	def __init__(self) -> None:
		self.default_url = 'https://server.growatt.com'
		self.req_session = requests.Session()

	def request(self, method, endpoint, *args, **kwargs):
		"""
			Makes the requests to the API with header and response handling.
			
			The function makes the request to the API url defined in the class initialization, handles the headers and converts the response to json.	

			Parameters:
			method (string): HTTP method in string 
			endpoint (string): api endpoint
			args: default args of requests lib
			kwargs: default kwargs of request lib
			
			Returns:
			response JSON data.
		"""
		
		url = self.default_url + endpoint
		self.req_session.headers.update({'Referer': url})
		print('\n> ' + method + ' ' + url)
		res = self.req_session.request(url=url, method=method, *args, **kwargs, headers={
			'Accept': '*/*',
      'User-Agent': 'client',
      'Content-Type': 'application/json',
		})
		print(res)
		try:
			resJSON = res.json()
			return resJSON
		except:
			print(res.text)
			raise Exception('JSON expected.')
	
	def handle_login(self, path, method, params=None):
		res = self.req_session.request(url=path, method=method, params=params, headers={
			'Accept': '*/*',
      'User-Agent': 'client',
      'Content-Type': 'application/json',
		})

		if not res.ok:
			raise Exception("Failed login attempt.")
		
		cookies = res.headers['Set-Cookie']
		
		self.req_session.headers.update({'cookie': cookies})
		return cookies	
	
	def login_test_user(self) -> str:
		"""
			Performs test login provided by growatt
			
			The login to the system is performed using growatt's default test login route, which returns session cookies that are stored in the requests library header.
			
			Returns:
			session cookie.
		"""
		endpoint = endpoints['login']['test']
		return self.handle_login(self.default_url+endpoint['path'], endpoint['method'])
	
	def login(self, username, password) -> str:
		endpoint = endpoints['login']['default']

		params = {
			'account': username,
			'password': password,
			'validateCode': ''
		}
		return self.handle_login(self.default_url+endpoint['path'], endpoint['method'], params)

	def get_plant_list_title(self):
		"""
			Returns properties of all plants of the current user.
			properties:
				timezone: number string
				id: number string
				plantName: string

			Returns:
			List with plants properties.
			Example:
			[{'timezone': '1', 'id': '11111', 'plantName': 'Res*************t'}]
		"""
		endpoint = endpoints['index']['get_plant_list_title']
		res = self.request(endpoint['method'], endpoint['path'])
		return res
	
	def get_plant_list(self, curr_page = '0', order_type = '-1', plant_name = '', plant_type = '-1'):
		"""
			In paging format, returns a plant list with the following properties:
				eToday: number string
				accountName: string
				plantImg: string
				id: number string
				onlineNum: number string
				plantName string
				plantType: number string
				currentPac: number string
			
			Parameters:
			currPage (number string): filter by page
			plantType (string): filter by plant type
			orderType (string number): filter by order type
			plantName (string): filter by name

			Returns:
			List with pagination data with plant properties.
			Example:
			{'currPage': 1, 'pages': 1, 'pageSize': 20, 'count': 3, 'ind': 1, 'datas': [{'eToday': '13.8', 'accountName': 'gue***t', 'plantImg': 'null', 'id': '1799541', 'onlineNum': '2', 'plantName': 'Res*************t', 'plantType': '0', 'currentPac': '1.3'}, {'eToday': '7', 'accountName': 'gue***t', 'plantImg': '1717443_1images1674494098.jpg', 'id': '1717443', 'onlineNum': '2', 'plantName': 'Ene****************t', 'plantType': '0', 'currentPac': '0.25'}, {'eToday': '2288.7', 'accountName': 'gue***t', 'plantImg': 'null', 'id': '1871831', 'onlineNum': '2', 'plantName': 'Com************t', 'plantType': '1', 'currentPac': '550.34'}], 'notPager': False}
		"""
		endpoint = endpoints['select_plant']['get_plant_list']

		params = {
			'currPage': curr_page,
			'orderType': order_type,
			'plantName': plant_name,
			'plantType': plant_type
		}
		res = self.request(endpoint['method'], endpoint['path'], params=params)
		return res
	
	def get_inv_total_data(self, plant_id, serial_number):
		"""
			Return all total data of inverters:
			eToday: string number
			eTotal: string number
			mTotal: string number
			mUnitText: string
			mToday: string number
			plantId: string number

			Parameters:
			sn (string): serial number
			plant_id (string number): plant id

			Returns:
			All total data of inverter

			Example:
			{
				"result": 1,
				"obj": {
					"eToday": "0",
					"eTotal": "0",
					"mTotal": "0",
					"mUnitText": "￡",
					"mToday": "0",
					"plantId": "1881857"
				}
			}
		"""
		params = {
			"plantId": plant_id,
			"sn": serial_number
		}
		endpoint = endpoints['panel']['inv']['get_total_data']
		res = self.request(endpoint['method'], endpoint['path'], params=params)
		return res
	
	def get_max_history(self, max_sn, start_date, end_date, start=0):
		"""
			Return history of max inverter.
			
			Parameters:
			max_sn (string): sn
			start_date (string): YYYY-MM-DD date format
			end_date (string): YYYY-MM-DD date format
			start (string number): idk

			Returns:
			History of max inverter with 10 days.

			Example:
			{
				"result": 1,
				"obj": {
					"endDate": "2023-05-03",
					"datas": [],
					"start": 0,
					"haveNext": false
				}
			}
		"""
		params = {
			"startDate": start_date,
			"endDate": end_date,
			"start": start,
			"maxSn": max_sn
		}
		endpoint = endpoints['device']['get_max_history']
		res = self.request(endpoint['method'], endpoint['path'], params=params)
		return res
		
	def get_devices_by_plant_list(self, plant_id, curr_page='0'):
		"""
			In paging format, returns the device (inverter) list with the following properties:
				deviceType: string number
				ptoStatus: string number
				timeServer: string date
				accountName: string
				eTotal: string number
				pac: string number
				dataLogSN: string
				alias: string
				location: string
				deviceModel: string
				sn: string
				plantName: string
				status: string number
				lastUpdateTime: string date

			Parameters:
			plant_id (number string): filter by plant_id
			curr_page (number string): filter by curr_page

			Returns:
			List with pagination data with devices properties.
			Example:
			{'result': 1, 'obj': {'currPage': 1, 'pages': 1, 'pageSize': 4, 'count': 1, 'ind': 1, 'datas': [{'deviceType': '1', 'ptoStatus': '288', 'timeServer': '2023-07-04 01:24:44', 'accountName': 'gue***t', 'timezone': '2', 'plantId': '1621427', 'deviceTypeName': 'tlx', 'nominalPower': '3600', 'bdcStatus': '1', 'eToday': '15.2', 'eMonth': '41.4', 'datalogTypeTest': 'ShineLanBox', 'eTotal': '2534.1', 'pac': '284', 'datalogSn': 'KWK1CE91AY', 'alias': 'SKL0CHS0CW', 'location': '', 'deviceModel': 'EU MIN 3600TL-XH', 'sn': 'SKL0CHS0CW', 'plantName': 'Res*************t', 'status': '1', 'lastUpdateTime': '2023-07-03 19:24:44'}], 'notPager': False}}
		"""
		params = {
			'plantId': plant_id,
			'currPage': curr_page
		}
		endpoint = endpoints['panel']['get_devices_by_plant']
		res = self.request(endpoint['method'], endpoint['path'], params=params)
		return res
	
	def get_plant_data(self, plant_id):
		"""
			Returns plant properties.
			
			Parameters:
			plant_id (string): plant id

			Returns:
			Plant properties.
			Example:
			{'result': 1, 'obj': {'country': 'Spain', 'formulaCo2': '0.4', 'accountName': 'Man***i', 'city': 'Fregenal de la Sierra', 'timezone': '2', 'co2': '1990.5', 'creatDate': '2022-11-07', 'formulaCoal': '0.4', 'designCompany': 'Electromati', 'fixedPowerPrice': '0.0', 'id': '1582054', 'lat': '38.165349', 'valleyPeriodPrice': '0.0', 'lng': '-6.654544', 'locationImg': 'null', 'tree': '274', 'peakPeriodPrice': '0.0', 'plantType': '0', 'nominalPower': '6300', 'formulaMoney': '1.2', 'formulaTree': '0.0', 'flatPeriodPrice': '0.0', 'eTotal': '4976.3', 'plantImg': '1582054_1images1668849333964.jpg', 'isShare': 'false', 'coal': '1990.5', 'moneyUnit': 'rmb', 'plantName': 'MAN***I', 'moneyUnitText': '￥'}}
		"""
		endpoint = endpoints['panel']['get_plant_data']
		res = self.request(endpoint['method'], endpoint['path'] + '?plantId=' + plant_id)
		return res