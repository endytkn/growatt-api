from GrowattApi import GrowattApi

class App:
  def __init__(self) -> None:
    self.growattApi = GrowattApi()
    self.plant_id = ''
    self.device_sn = ''

  def print_title(self, title):
    print("\n" + title + ':')

  def get_all_plants_title(self):
    plants = self.growattApi.get_plant_list_title()
    if (len(plants) == 0):
      print('No plants!')
    self.print_title("ALL PLANTS TITLE")
    print(plants)
    first_plant = plants[0]
    self.plant_id = first_plant['id']

  def get_all_plants(self):
    plants = self.growattApi.get_plant_list()
    self.print_title("ALL PLANTS")
    print(plants)

  def get_plant_data(self):
    plant_data = self.growattApi.get_plant_data(self.plant_id)
    self.print_title("PLANT PROPS")
    print(plant_data)

  def get_device(self):
    devices_res = self.growattApi.get_devices_by_plant_list(self.plant_id)
    self.print_title("DEVICES")
    print(devices_res)
    devices = devices_res['obj']['datas']
    device = devices[0]
    self.print_title("FIRST DEVICE")
    print(device)
    self.device_sn = device['sn']

  def get_inversor_total_data(self):
    inversor_total_data = self.growattApi.get_inv_total_data(self.plant_id, self.device_sn)
    self.print_title("INVERSOR TOTAL DATA")
    print(inversor_total_data)

  def get_max_history(self):
    max_history = self.growattApi.get_max_history(self.device_sn, '2023-04-04', '2023-04-10')
    self.print_title("MAX HISTORY")
    print(max_history)


  def start(self) -> None:
    self.growattApi.login(username='', password='')
    self.get_all_plants()
    self.get_all_plants_title()
    self.get_plant_data()
    self.get_device()
    self.get_inversor_total_data()
    self.get_max_history()
    

if __name__=="__main__":
  app = App()
  app.start()