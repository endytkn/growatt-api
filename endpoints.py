growatt_endpoints = {
    "device": {
			"get_max_history": {
				"path": "/device/getMAXHistory",
				"method": "POST"
			},
    	"get_datalog_history": {
				"path": "/device/getDatalogHistory",
				"method": "POST"
			}
		},
    "inverter": {
        "get_inverter_data": {
					"path": "/inverter/getInverterData",
					"method": "POST"
				}
		},
    "panel": {
        "inv": {
            "get_total_data": {
							"path": "/panel/inv/getInvTotalData",
							"method": "POST"
						}
				},
				"max": {
            "get_total_data": {
							"path": "/panel/max/getMAXTotalData",
							"method": "POST"
						}
				},
        "mix": {
            "get_total_data": {
							"path": "/panel/mix/getMIXTotalData",
							"method": "POST"
						}
				},
        "get_devices_by_plant": {
					"path": "/panel/getDevicesByPlantList",
					"method": "POST"
				},
        "get_plant_data": {
					"path": "/panel/getPlantData",
					"method": "POST"
				}
		},
		"select_plant": {
      "get_plant_list": {
				"path": "/selectPlant/getPlantList",
				"method": "POST"
			}
		},
    "index": {
        "get_plant_list_title": {
					"path": "/index/getPlantListTitle",
					"method": "POST"
				}
		},
    "login": {
        "test": {
					"path": "/login/toViewExamlePlant",
					"method": "GET"
				},
        "default": {
            "path": "/login",
            "method": "POST"
				}
		}
}